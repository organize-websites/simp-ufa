// ignore_for_file: unnecessary_null_comparison, avoid_print, prefer_const_constructors

import 'dart:async';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:simp/usr2/ferramentas/b2cor2.dart';
import '../usr2/ferramentas/painel2.dart';
import '../usr2/ferramentas/simp2.dart';
import '../usr2/ferramentas/treinacor2.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../absoluta.dart';
import '../alerta.dart';
import 'addusr2.dart';

import 'cardcor2.dart';
import 'ferramentas/ajuda2.dart';
import 'ferramentas/appcor2.dart';
import 'ferramentas/controlcor2.dart';
import 'ferramentas/mktcor2.dart';
import 'ferramentas/sivcor2.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

bool isLoading = true;
bool isM = true;
bool isB2cor = true;

late InAppWebViewController _webViewController;

class Abs2 extends StatefulWidget {
  const Abs2({Key? key}) : super(key: key);


  static Future<String> get _urlb async { 

  var prefs2 = await SharedPreferences.getInstance();
  String usertoken = (prefs2.getString("usr_tkn2") ?? "");


    await Future.delayed(const Duration(seconds: 1));
    return usertoken;
  }

  static Future<String?> get _usrnm async {

  var prefs = await SharedPreferences.getInstance();
  String nome = (prefs.getString("nome2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return nome;
  }

  static Future<String?> get _usrimg async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return logotipousuario;
  }  

  static Future<String?> get _uslog async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return login;
  }  

  static Future<String?> get _usrnm2 async {

  var prefs = await SharedPreferences.getInstance();
  String nome = (prefs.getString("nome")!);

    await Future.delayed(const Duration(seconds: 1));
    if(nome == null){
      return null;
    }
    else{
    return nome;
    }
  }

  static Future<String?> get _usrimg2 async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return logotipousuario;
  }  

  static Future<String?> get _uslog2 async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return login;
  }    


  static Future<String?> get _b2cor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("b2cor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _simp async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("simp2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _cardcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("cardcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _mktcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("mktcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _appcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("appcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<bool?> get _sivcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("sivcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<bool?> get _controlcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("controlcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<String?> get _appControle async {
  var prefs = await SharedPreferences.getInstance();
  String cartao = (prefs.getString("appControle2")!);
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'sivcor'){
      return cartao;
    }
    else{
      return null;
    }
  }

  static Future<String?> get _appControle2 async {
  var prefs = await SharedPreferences.getInstance();
  String cartao = (prefs.getString("appControle2")!);
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'controlcor'){
      return cartao;
    }
    else{
      return null;
    }
  }



  @override
  _Abs2State createState() => _Abs2State();
}

class _Abs2State extends State<Abs2> { 

   

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    super.initState();
    setState(() {
      isM = true;
      isB2cor = true;
      isLoading = true;
    });
  }

  clearUsr() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();  
  
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () 
    async{
      if (_webViewController != null) {
        _webViewController.goBack();
      }
      return true;
    },
    child: Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            isB2cor ?
            Center(
              child:FutureBuilder(
                future: Abs2._urlb,
                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                ? Stack(
                  children: [
                    InAppWebView( 
                      initialOptions: InAppWebViewGroupOptions(
                        android: AndroidInAppWebViewOptions(
                          useHybridComposition: true,
                          useOnRenderProcessGone: true,
                          useShouldInterceptRequest: true,
                          useWideViewPort: true,
                        ),
                      ),
                      onWebViewCreated: (InAppWebViewController controller){
                        _webViewController = controller;
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onLoadStart: (_controller, url) {
                        setState(() {
                          isLoading = true;
                        });
                        if (url!.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/simp_antigo')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                      },
                      onLoadStop: (_controller, url){
                        setState(() {
                          isLoading = false;
                        });
                      },
                      initialUrlRequest: URLRequest(url: Uri.parse('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp')),
                      onLoadHttpError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      },
                      onLoadError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      }
                    ),
                    isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,))) : Stack()
                  ],
                )
                : Image.asset('imagens/2.gif', width: 150, height: 150,)),
            ) : Stack(),
            if (isM) Stack(
              children: [
                Center(child: Container(color: Color.fromRGBO(0, 0, 0, 0.5),)),
                ClipRRect(
                borderRadius: BorderRadius.only(bottomRight: Radius.circular(12), topRight: Radius.circular(12)),
                  child: Drawer(
                    child: Container(
                      color: Colors.white,
                      child: 
                      CustomScrollView(
                        slivers: [
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Padding(
                                  padding: const EdgeInsets.only(top: 30, bottom: 0, right: 80, left: 80),
                                  child: Image.asset('imagens/logo.png', width: 100, height: 100,),
                                ),
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Center(
                                  child: FutureBuilder(
                                    future: Abs2._usrnm2,
                                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                    ? Column(
                                      children: [
                                        ExpandablePanel(
                                          header: 
                                          ListTile(
                                            leading: FutureBuilder(
                                              future: Abs2._usrimg,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Stack(
                                                children: [
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                                ],
                                              )
                                              : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                            ),
                                            title: FutureBuilder(
                                              future: Abs2._usrnm,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                              : const CircularProgressIndicator()
                                            ),
                                            subtitle: FutureBuilder(
                                              future: Abs2._uslog,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                              : const CircularProgressIndicator()
                                            ),
                                          ),
                                          expanded: 
                                          ListTile(
                                            leading: FutureBuilder(
                                              future: Abs2._usrimg2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Stack(
                                                children: [
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                                ],
                                              )
                                              : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                            ),
                                            title: FutureBuilder(
                                              future: Abs2._usrnm2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                              : const CircularProgressIndicator()
                                            ),
                                            subtitle: FutureBuilder(
                                              future: Abs2._uslog2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                              : const CircularProgressIndicator()
                                            ),
                                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Abs()));},
                                          ), 
                                          collapsed: const Center(),
                                        ),
                                        Center(
                                          child:
                                          TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: Text('Trocar Usuário Extra'))
                                        )
                                      ],
                                    ) :
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        ListTile(
                                          leading: FutureBuilder(
                                            future: Abs2._usrimg,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Stack(
                                              children: [
                                                Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                              ],
                                            )
                                            : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                          ),
                                          title: FutureBuilder(
                                            future: Abs2._usrnm,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                            : const CircularProgressIndicator()
                                          ),
                                          subtitle: FutureBuilder(
                                            future: Abs2._uslog,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                            : const CircularProgressIndicator()
                                          ),
                                        ),
                                        Center(
                                          child:
                                          TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: Text('Adicionar Usuário Extra'))
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverGrid(
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3), 
                            delegate: SliverChildListDelegate(
                              [
                                //_simp
                                FutureBuilder(
                                future: Abs2._simp,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){
                                      setState(() {
                                        isM = false;
                                        isB2cor = true;
                                      });
                                    },
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/simp.png', width: 50, height: 50,),
                                        Text('SIMP', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/simpoff.png', width: 50, height: 50,),
                                        Text('SIMP', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_b2cor
                                FutureBuilder(
                                future: Abs2._b2cor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => B2cor2()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/b2cor.png', width: 50, height: 50,),
                                        Text('B2Cor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/b2coroff.png', width: 50, height: 50,),
                                        Text('B2Cor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_cardcor
                                FutureBuilder(
                                future: Abs2._cardcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const CardCor2(title: '',)));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/cardcor.png', width: 50, height: 50,),
                                        Text('CardCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/cardcoroff.png', width: 50, height: 50,),
                                        Text('CardCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_mktcor
                                FutureBuilder(
                                future: Abs2._mktcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Mktcor2()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/mktcor.png', width: 50, height: 50,),
                                        Text('MktCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/mktcoroff.png', width: 50, height: 50,),
                                        Text('MktCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_appcor
                                FutureBuilder(
                                future: Abs2._appcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Appcor2()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/appcor.png', width: 50, height: 50,),
                                        Text('AppCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/appcoroff.png', width: 50, height: 50,),
                                        Text('AppCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ),
                                //_sivcor
                                FutureBuilder(
                                future: Abs2._sivcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Sivcor2()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/sivcor.png', width: 50, height: 50,),
                                        Text('SivCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : //_controlcor
                                  FutureBuilder(
                                  future: Abs2._controlcor,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? TextButton(
                                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Controlcor2()));},
                                      child: 
                                      Column(
                                        children: [
                                          Image.asset('imagens/ferramentas/controlcor.png', width: 50, height: 50,),
                                          Text('ControlCor', style: TextStyle(color: Colors.grey[700]))
                                        ],
                                      )
                                    )
                                  : //_sivcorff
                                    FutureBuilder(
                                    future: Abs2._appControle,
                                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                    ? TextButton(
                                        onPressed: null,
                                        child: 
                                        Column(
                                          children: [
                                            Image.asset('imagens/ferramentas/sivcoroff.png', width: 50, height: 50,),
                                            Text('SivCor', style: TextStyle(color: Colors.grey[400]))
                                          ],
                                        )
                                      )
                                    : //_controlcoroff
                                      FutureBuilder(
                                      future: Abs2._appControle2,
                                      builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                      ? TextButton(
                                          onPressed: null,
                                          child: 
                                          Column(
                                            children: [
                                              Image.asset('imagens/ferramentas/controlcoroff.png', width: 50, height: 50,),
                                              Text('ControlCor', style: TextStyle(color: Colors.grey[400]))
                                            ],
                                          )
                                        )
                                      : Visibility(child: Text(""), visible: false,)
                                      ),
                                    ), 
                                  ),
                                ), 
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                ListTile(
                                  leading: Image.asset('imagens/sup.png', width: 35, height: 35,),
                                  title: Text('Suporte - Base de Conhecimento', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Ajuda2()));}, 
                                ),
                                ListTile(
                                  leading: Image.asset('imagens/uni.png', width: 35, height: 35,),
                                  title: Text('Universidade Digital', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Treinacor2()));}, 
                                ),
                                ListTile(
                                  leading: Image.asset('imagens/cli.png', width: 35, height: 35,),
                                  title: Text('Painel do Cliente', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Painel2()));}, 
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: 
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Image.asset('imagens/ag.png', width: 102, height: 50,)
                                    ],
                                  ),
                                ),
                              ]
                            )
                          ),
                        ], 
                      ),
                    ),
                  ),
                ),
              ],
            ) else Stack(),
          ],
        ),
      ),
      bottomNavigationBar: 
      ConvexAppBar(
        curveSize: 100,
        top: -15,
        style: TabStyle.fixedCircle,
        backgroundColor: Color.fromRGBO(52, 58, 64, 1),
        items: 
        [
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.home, color: Colors.white,), 
              onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));}, 
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goBack();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.apps, color: Colors.white,),
              onPressed: (){
                setState(() 
                  {
                    isM = true;
                    isB2cor = true;
                  }
                );
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goForward();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
              onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
            )
          ),
        ],
      )
    ),
  );
}