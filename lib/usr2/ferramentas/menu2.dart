// ignore_for_file: unnecessary_string_interpolations, prefer_const_constructors, unnecessary_null_comparison

import '../../usr2/addusr2.dart';
import '../../usr2/cardcor2.dart';
import '../../usr2/ferramentas/appcor2.dart';
import '../../usr2/ferramentas/b2cor2.dart';
import '../../usr2/ferramentas/controlcor2.dart';
import '../../usr2/ferramentas/mktcor2.dart';
import '../../usr2/ferramentas/sivcor2.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../absoluta.dart';
import 'ajuda2.dart';
import 'painel2.dart';
import 'simp2.dart';
import 'treinacor2.dart';

class Menu2 extends StatefulWidget{
  const Menu2({Key? key}) : super(key: key);


  static Future<String?> get _usrnm async {

  var prefs = await SharedPreferences.getInstance();
  String nome = (prefs.getString("nome2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return '$nome';
  }

  static Future<String?> get _usrimg async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return '$logotipousuario';
  }  

  static Future<String?> get _uslog async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return '$login';
  }  

  static Future<String?> get _usrnm2 async {

  var prefs = await SharedPreferences.getInstance();
  String nome = (prefs.getString("nome")!);

    await Future.delayed(const Duration(seconds: 1));
    if(nome == null){
      return null;
    }
    else{
    return '$nome';
    }
  }

  static Future<String?> get _usrimg2 async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return '$logotipousuario';
  }  

  static Future<String?> get _uslog2 async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return '$login';
  }    


  static Future<String?> get _b2cor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("b2cor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _simp async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("simp2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _cardcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("cardcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _mktcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("mktcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _appcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("appcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<bool?> get _sivcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("sivcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<bool?> get _controlcor async {

  var prefs = await SharedPreferences.getInstance();
  bool cartao = (prefs.getBool("controlcor2")!);

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<String?> get _appControle async {
  var prefs = await SharedPreferences.getInstance();
  String cartao = (prefs.getString("appControle2")!);
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'sivcor'){
      return '$cartao';
    }
    else{
      return null;
    }
  }

  static Future<String?> get _appControle2 async {
  var prefs = await SharedPreferences.getInstance();
  String cartao = (prefs.getString("appControle2")!);
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'controlcor'){
      return '$cartao';
    }
    else{
      return null;
    }
  }


  @override
  _Menu2State createState() => _Menu2State();
}

class _Menu2State extends State<Menu2> {
  @override
  Widget build(BuildContext context){
    return
      ClipRRect(
    borderRadius: BorderRadius.only(bottomRight: Radius.circular(12), topRight: Radius.circular(12)),
      child: Drawer(
        child: Container(
          color: Colors.white,
          child: 
          CustomScrollView(
            slivers: [
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(top: 30, bottom: 0, right: 80, left: 80),
                      child: Image.asset('imagens/logo.png', width: 100, height: 100,),
                    ),
                  ]
                )
              ),
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(left: 15,right: 15),
                      child: Divider(),
                    ),   
                  ]
                )
              ),
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    Center(
                      child: FutureBuilder(
                        future: Menu2._usrnm2,
                        builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                        ? Column(
                          children: [
                            ExpandablePanel(
                              header: 
                              ListTile(
                                leading: FutureBuilder(
                                  future: Menu2._usrimg,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Stack(
                                    children: [
                                      Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                      Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                    ],
                                  )
                                  : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                ),
                                title: FutureBuilder(
                                  future: Menu2._usrnm,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                  : const CircularProgressIndicator()
                                ),
                                subtitle: FutureBuilder(
                                  future: Menu2._uslog,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                  : const CircularProgressIndicator()
                                ),
                              ),
                              expanded: 
                              ListTile(
                                leading: FutureBuilder(
                                  future: Menu2._usrimg2,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Stack(
                                    children: [
                                      Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                      Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                    ],
                                  )
                                  : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                ),
                                title: FutureBuilder(
                                  future: Menu2._usrnm2,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                  : const CircularProgressIndicator()
                                ),
                                subtitle: FutureBuilder(
                                  future: Menu2._uslog2,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                  : const CircularProgressIndicator()
                                ),
                                onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Abs()));},
                              ), 
                              collapsed: const Center(),
                            ),
                            Center(
                              child:
                              TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: Text('Trocar Usuário Extra'))
                            )
                          ],
                        ) :
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ListTile(
                              leading: FutureBuilder(
                                future: Menu2._usrimg,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? Stack(
                                  children: [
                                    Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                    Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                  ],
                                )
                                : Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                              ),
                              title: FutureBuilder(
                                future: Menu2._usrnm,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? Text(snapshot.data, style: TextStyle(fontSize: 14),)
                                : const CircularProgressIndicator()
                              ),
                              subtitle: FutureBuilder(
                                future: Menu2._uslog,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? Text(snapshot.data, style: TextStyle(fontSize: 10),)
                                : const CircularProgressIndicator()
                              ),
                            ),
                            Center(
                              child:
                              TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: Text('Adicionar Usuário Extra'))
                            )
                          ],
                        ),
                      ),
                    )
                  ]
                )
              ),
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(left: 15,right: 15),
                      child: Divider(),
                    ),   
                  ]
                )
              ),
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3), 
                delegate: SliverChildListDelegate(
                  [
                    //_simp
                    FutureBuilder(
                    future: Menu2._simp,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Simp2()));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/simp.png', width: 50, height: 50,),
                            Text('SIMP', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : TextButton(
                        onPressed: null,
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/simpoff.png', width: 50, height: 50,),
                            Text('SIMP', style: TextStyle(color: Colors.grey[200]))
                          ],
                        )
                      )
                    ), 
                    //_b2cor
                    FutureBuilder(
                    future: Menu2._b2cor,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => B2cor2()));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/b2cor.png', width: 50, height: 50,),
                            Text('B2Cor', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : TextButton(
                        onPressed: null,
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/b2coroff.png', width: 50, height: 50,),
                            Text('B2Cor', style: TextStyle(color: Colors.grey[200]))
                          ],
                        )
                      )
                    ), 
                    //_cardcor
                    FutureBuilder(
                    future: Menu2._cardcor,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const CardCor2(title: '',)));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/cardcor.png', width: 50, height: 50,),
                            Text('CardCor', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : TextButton(
                        onPressed: null,
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/cardcoroff.png', width: 50, height: 50,),
                            Text('CardCor', style: TextStyle(color: Colors.grey[200]))
                          ],
                        )
                      )
                    ), 
                    //_mktcor
                    FutureBuilder(
                    future: Menu2._mktcor,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Mktcor2()));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/mktcor.png', width: 50, height: 50,),
                            Text('MktCor', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : TextButton(
                        onPressed: null,
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/mktcoroff.png', width: 50, height: 50,),
                            Text('MktCor', style: TextStyle(color: Colors.grey[200]))
                          ],
                        )
                      )
                    ), 
                    //_appcor
                    FutureBuilder(
                    future: Menu2._appcor,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Appcor2()));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/appcor.png', width: 50, height: 50,),
                            Text('AppCor', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : TextButton(
                        onPressed: null,
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/appcoroff.png', width: 50, height: 50,),
                            Text('AppCor', style: TextStyle(color: Colors.grey[200]))
                          ],
                        )
                      )
                    ),
                    //_sivcor
                    FutureBuilder(
                    future: Menu2._sivcor,
                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                    ? TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Sivcor2()));},
                        child: 
                        Column(
                          children: [
                            Image.asset('imagens/ferramentas/sivcor.png', width: 50, height: 50,),
                            Text('SivCor', style: TextStyle(color: Colors.grey[700]))
                          ],
                        )
                      )
                    : //_controlcor
                      FutureBuilder(
                      future: Menu2._controlcor,
                      builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                      ? TextButton(
                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Controlcor2()));},
                          child: 
                          Column(
                            children: [
                              Image.asset('imagens/ferramentas/controlcor.png', width: 50, height: 50,),
                              Text('ControlCor', style: TextStyle(color: Colors.grey[700]))
                            ],
                          )
                        )
                      : //_sivcorff
                        FutureBuilder(
                        future: Menu2._appControle,
                        builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                        ? TextButton(
                            onPressed: null,
                            child: 
                            Column(
                              children: [
                                Image.asset('imagens/ferramentas/sivcoroff.png', width: 50, height: 50,),
                                Text('SivCor', style: TextStyle(color: Colors.grey[400]))
                              ],
                            )
                          )
                        : //_controlcoroff
                          FutureBuilder(
                          future: Menu2._appControle2,
                          builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                          ? TextButton(
                              onPressed: null,
                              child: 
                              Column(
                                children: [
                                  Image.asset('imagens/ferramentas/controlcoroff.png', width: 50, height: 50,),
                                  Text('ControlCor', style: TextStyle(color: Colors.grey[400]))
                                ],
                              )
                            )
                          : Visibility(child: Text(""), visible: false,)
                          ),
                        ), 
                      ),
                    ), 
                  ]
                )
              ),
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(left: 15,right: 15, top: 40),
                      child: Divider(),
                    ),   
                  ]
                )
              ),
              SliverList(
                delegate: 
                SliverChildListDelegate(
                  [
                    ListTile(
                      leading: Image.asset('imagens/sup.png', width: 35, height: 35,),
                      title: Text('Suporte - Base de Conhecimento', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Ajuda2()));}, 
                    ),
                    ListTile(
                      leading: Image.asset('imagens/uni.png', width: 35, height: 35,),
                      title: Text('Universidade Digital', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Treinacor2()));}, 
                    ),
                    ListTile(
                      leading: Image.asset('imagens/cli.png', width: 35, height: 35,),
                      title: Text('Painel do Cliente', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Painel2()));}, 
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: 
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset('imagens/ag.png', width: 102, height: 50,)
                        ],
                      ),
                    ),
                  ]
                )
              ),
            ],
          ),
        ),
      ),
    );
  }
}