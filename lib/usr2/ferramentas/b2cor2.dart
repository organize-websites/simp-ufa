// ignore_for_file: avoid_print

import 'dart:async';

import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../alerta.dart';
 

import 'menu2.dart';

bool isLoading = true;

late InAppWebViewController _webViewController;
late InAppWebViewController _webViewPopupController;

class B2cor2 extends StatefulWidget {
  const B2cor2({Key? key}) : super(key: key);


  static Future<String> get _urlB2cor async {

  var prefs2 = await SharedPreferences.getInstance();
  String usertoken = (prefs2.getString("usr_tkn2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return usertoken;
  }

  @override
  _B2cor2State createState() => _B2cor2State();
}

class _B2cor2State extends State<B2cor2> { 

   

  navOutra() {
    setState(() {
      isLoading = false;
    });
  }

  paraOutraTela() async{
    var _duration = const Duration(seconds: 6);
    return Timer(_duration, navOutra);
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
     super.initState();
    setState(() {
          isLoading = true;
        });
    paraOutraTela();
  }

  clearUsr() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>(); 
  final GlobalKey<ScaffoldState> _scaffoldKey2 = GlobalKey<ScaffoldState>();  
  
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () 
    async{
      // ignore: unnecessary_null_comparison
      if (_webViewController != null) {
        _webViewController.goBack();
      }
      return true;
    },
    child: Scaffold(
      key: _scaffoldKey,
      body: 
      Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Center(
              child:FutureBuilder(
                future: B2cor2._urlB2cor,
                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                ? Stack(
                  children: [
                    InAppWebView(
                      initialOptions: InAppWebViewGroupOptions(
                        android: AndroidInAppWebViewOptions(
                          useHybridComposition: true,
                          useOnRenderProcessGone: true,
                          useShouldInterceptRequest: true,
                          useWideViewPort: true,
                          supportMultipleWindows: true,
                        ),
                        crossPlatform: InAppWebViewOptions(
                          // set this to true if you are using window.open to open a new window with JavaScript
                          javaScriptCanOpenWindowsAutomatically: true
                        ),
                      ), 
                      onWebViewCreated: (InAppWebViewController controller){
                        _webViewController = controller;
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onCreateWindow: (controller, createWindowRequest) async {

                        var wi = MediaQuery.of(context).size.width;
                        var hi = 400.0;

                        print("onCreateWindow");

                        showDialog(
                          context: context,
                          builder: (context) {
                            return  Scaffold(
                              key: _scaffoldKey2,
                              body:  
                                SizedBox(
                                  width: MediaQuery.of(context).size.width * 100,
                                  height: MediaQuery.of(context).size.height * 50,
                                  child: InAppWebView(
                                    initialOptions: InAppWebViewGroupOptions(
                                      android: AndroidInAppWebViewOptions(
                                        useHybridComposition: true,
                                        useOnRenderProcessGone: true,
                                        useShouldInterceptRequest: true,
                                        useWideViewPort: true,
                                      ),
                                      crossPlatform: InAppWebViewOptions(
                                        // set this to true if you are using window.open to open a new window with JavaScript
                                        javaScriptCanOpenWindowsAutomatically: true
                                      ),
                                    ),
                                    // Setting the windowId property is important here!
                                    windowId: createWindowRequest.windowId,
                                    onWebViewCreated: (InAppWebViewController controller2) {
                                      _webViewPopupController = controller2;
                                    },
                                    onLoadStart: (InAppWebViewController controller2, Uri? url) {
                                      String ur2 = url.toString();
                                      print("onLoadStart popup $url");
                                      if (ur2.contains('about:blank') || ur2.contains('whatsapp')) {
                                        
                                      }
                                      else {
                                        setState(() {
                                          wi = MediaQuery.of(context).size.width * 100;
                                        });
                                        setState(() {
                                          hi = MediaQuery.of(context).size.height * 100;
                                        });
                                      }
                                      if (url!.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                                        );          
                                      }
                                      else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/simp_antigo')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                                        );          
                                      }
                                      else if (url.toString().contains('https://app.simp.agencialink.com/')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                                        );          
                                      }
                                      else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tabelas')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                                        );  
                                      }
                                      else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                                        );  
                                      }
                                      else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tradicional')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                                        );  
                                      }
                                      else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                                        );  
                                      }
                                      else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/moderno')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                                        );  
                                      }
                                      else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                                        );  
                                      }
                                      else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/profissional')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                                        );  
                                      }
                                      else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional')) {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                                        launch(
                                          'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                                        );  
                                      }
                                    },
                                    onLoadStop: (InAppWebViewController controller2, Uri? url) {
                                      print("onLoadStop popup $url");
                                    },
                                    onLoadError: (InAppWebViewController controller2, Uri? url, int i, String s) async {
                                      String url2 = url.toString();
                                      print('CUSTOM_HANDLER: $i, $s');
                                      String action = url2.split(':').first;
                                      List<String> customActions2 = ['tel', 'whatsapp', 'mailto'];
                                      bool isCustomAction2 = customActions2.contains(action);
                                      if (isCustomAction2) {
                                        if (url2.contains('whatsapp://send/')) {
                                          String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                                          await launch(
                                            ur3,
                                          );
                                          Navigator.of(context, rootNavigator: true).pop();
                                        }
                                        else if (await canLaunch(url2)) {
                                          await launch(
                                            url2,
                                          );
                                          Navigator.of(context, rootNavigator: true).pop();
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      } 
                                      if (isCustomAction2) {
                                        if (url2.contains('https://api.whatsapp.com/send/')) {
                                          String ur3 = url2.replaceAll('https://api.whatsapp.com/send/', 'https://wa.me/');
                                          await launch(
                                            ur3,
                                          );
                                          Navigator.of(context, rootNavigator: true).pop();
                                        }
                                        else if (await canLaunch(url2)) {
                                          await launch(
                                            url2,
                                          );
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      }
                                    },
                                  ),
                              ),
                              drawer: const Menu2(),
                              bottomNavigationBar: 
                              ConvexAppBar(
                                curveSize: 100,
                                top: -15,
                                style: TabStyle.fixedCircle,
                                backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
                                items: 
                                [
                                  TabItem(
                                    icon: 
                                    IconButton(
                                      icon: const Icon(Icons.home, color: Colors.white,), 
                                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));},
                                    )
                                  ),
                                  TabItem(
                                    icon: 
                                    IconButton(
                                      icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
                                      onPressed: () 
                                      {
                                        _webViewController.goBack();
                                      },
                                    )
                                  ),
                                  TabItem(
                                    icon: 
                                    IconButton(
                                      icon: const Icon(Icons.apps, color: Colors.white,),
                                      onPressed: () => _scaffoldKey2.currentState!.openDrawer(),
                                    )
                                  ),
                                  TabItem(
                                    icon: 
                                    IconButton(
                                      icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
                                      onPressed: () 
                                      {
                                        _webViewController.goForward();
                                      },
                                    )
                                  ),
                                  TabItem(
                                    icon: 
                                    IconButton(
                                      icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
                                      onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
                                    )
                                  ),
                                ],
                              ) 
                            );
                          },
                        );

                        return true;
                      },
                      onLoadStart: (_controller, url){
                        setState(() {
                          isLoading = true;
                        });
                        if (url!.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/simp_antigo')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://app.simp.agencialink.com/')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                      },
                      onLoadStop: (_controller, url){
                        setState(() {
                          isLoading = false;
                        });
                      },
                      initialUrlRequest: URLRequest(url: Uri.parse('https://barra.agencialink.com.br/autologin/${snapshot.data}/b2cor')),
                      onLoadHttpError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      },
                      onLoadError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      }
                    ),
                    isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,))) : Stack()
                  ],
                )
                : Image.asset('imagens/2.gif', width: 150, height: 150,)
              ),
            ),
          ),
          isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,)))
          : Stack()
        ],
      ),
      drawer: const Menu2(),
      bottomNavigationBar: 
      ConvexAppBar(
        curveSize: 100,
        top: -15,
        style: TabStyle.fixedCircle,
        backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
        items: 
        [
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.home, color: Colors.white,), 
              onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor2()));},
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goBack();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.apps, color: Colors.white,),
              onPressed: () => _scaffoldKey.currentState!.openDrawer(),
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goForward();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
              onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
            )
          ),
        ],
      ) 
    ),
  );

  @override
  void dispose() {
    super.dispose();
  }
} 