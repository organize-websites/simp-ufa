// ignore_for_file: avoid_print

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../usr2/ferramentas/cadastrocardcor2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'api/apiremove.dart';
import 'ferramentas/cadastrocardcor.dart';
import 'mudausr.dart';

clearUsr() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.clear();
}

_onClickLogin(BuildContext context) async {
  await ApiR.login();
}

alert(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.sadTear, size: 90, color: Colors.red[700],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Oops...", style: TextStyle(color: Colors.red[700], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Tentar Novamente'))
        ]
      );
    }
  );
}

alert2(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.edit, size: 90, color: Colors.blue[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Oops...", style: TextStyle(color: Colors.blue[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Tentar Novamente'))
        ]
      );
    }
  );
}

alert3(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.edit, size: 90, color: Colors.red[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Bloqueado...", style: TextStyle(color: Colors.red[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Tentar Novamente'))
        ]
      );
    }
  );
}

alert4(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.exclamationTriangle, size: 90, color: Colors.yellow[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Atenção!!!", style: TextStyle(color: Colors.yellow[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Cancelar')),
          TextButton(onPressed: () {clearUsr(); _onClickLogin(context); Navigator.push(context, MaterialPageRoute(builder: (context) => const MudaUsr(title: '',)));}, child: const Text('Sair', style: TextStyle(color: Colors.red),)),
        ]
      );
    }
  );
}

alert5(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.exclamationTriangle, size: 90, color: Colors.yellow[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Atenção!!!", style: TextStyle(color: Colors.yellow[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Cancelar')),
          TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => const Cdc()));}, child: const Text('Aprenda aqui!', style: TextStyle(color: Colors.red),)),
        ]
      );
    }
  );
}

alert6(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.exclamationTriangle, size: 90, color: Colors.yellow[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Atenção!!!", style: TextStyle(color: Colors.yellow[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Cancelar')),
          TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => const Cdc2()));}, child: const Text('Aprenda aqui!', style: TextStyle(color: Colors.red),)),
        ]
      );
    }
  );
}

alertAddLead(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.check, size: 90, color: Colors.green[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Sucesso!!!", style: TextStyle(color: Colors.green[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Ok')),
        ]
      );
    }
  );
}

alertNoAddLead(BuildContext context, String msg){
  showDialog(
    context: context,
    builder: (context){
      return AlertDialog(
        title:
        Center(
          child: 
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FaIcon(FontAwesomeIcons.timesCircle, size: 90, color: Colors.red[900],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Erro", style: TextStyle(color: Colors.red[900], fontSize:25), textAlign: TextAlign.center,),
              ),
            ],
          )
        ),
        content: Text(msg, textAlign: TextAlign.center,),
        actions: <Widget>[
          TextButton(onPressed: () {Navigator.pop(context);}, child: const Text('Tentar Novamente'))
        ]
      );
    }
  );
}