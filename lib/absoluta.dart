// ignore_for_file: unnecessary_null_comparison, avoid_print

import 'dart:async';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:simp/ferramentas/menu.dart';
import '../cardcor.dart';
import '../ferramentas/ajuda.dart';
import '../ferramentas/painel.dart';
import '../ferramentas/simp.dart';
import '../ferramentas/treinacor.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'ferramentas/appcor.dart';
import 'ferramentas/b2cor.dart';
import 'ferramentas/controlcor.dart';
import 'ferramentas/mktcor.dart';
import 'ferramentas/sivcor.dart';
import 'usr2/absoluta2.dart';
import 'usr2/addusr2.dart';
import 'alerta.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';
    

bool isLoading = true;
bool isM = true;
bool isB2cor = true;

//Completer<WebViewController> _controller = Completer<WebViewController>();
late InAppWebViewController _webViewController;
late InAppWebViewController _webViewPopupController;

class Abs extends StatefulWidget {
  const Abs({Key? key}) : super(key: key);

  static Future<String> get _urlb async {

  var prefs2 = await SharedPreferences.getInstance();
  String usertoken = (prefs2.getString("usr_tkn") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return usertoken;
  }

  static Future<String?> get _usrnm async {

  var prefs = await SharedPreferences.getInstance();
  String nome = (prefs.getString("nome") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return nome;
  }

  static Future<String?> get _cdCor async {

  var prefs = await SharedPreferences.getInstance();
  String? cartao = (prefs.getString("cartao"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao == null){
      return null;
    }
    else{
    return cartao;
    }
  } 

  static Future<String?> get _usrimg async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return logotipousuario;
  }  

  static Future<String?> get _uslog async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return login;
  }  

  static Future<String?> get _usrnm2 async {

  var prefs = await SharedPreferences.getInstance();
  String? nome = (prefs.getString("nome2"));

    await Future.delayed(const Duration(seconds: 1));
    if(nome == null){
      return null;
    }
    else{
    return nome;
    }
  }

  static Future<String?> get _usrimg2 async {

  var prefs = await SharedPreferences.getInstance();
  String logotipousuario = (prefs.getString("logotipousuario2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return logotipousuario;
  }  

  static Future<String?> get _uslog2 async {

  var prefs = await SharedPreferences.getInstance();
  String login = (prefs.getString("login2") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    return login;
  }    


  static Future<String?> get _b2cor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("b2cor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _simp async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("simp"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _cardcor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("cardcor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _mktcor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("mktcor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<String?> get _appcor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("appcor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return '$cartao';
    }
  }

  static Future<bool?> get _sivcor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("sivcor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<bool?> get _controlcor async {

  var prefs = await SharedPreferences.getInstance();
  bool? cartao = (prefs.getBool("controlcor"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao != true){
      return null;
    }
    else{
    return cartao;
    }
  }

  static Future<String?> get _appControle async {
  var prefs = await SharedPreferences.getInstance();
  String? cartao = (prefs.getString("appControle"));
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'sivcor'){
      return cartao;
    }
    else{
      return null;
    }
  }

  static Future<String?> get _appControle2 async {
  var prefs = await SharedPreferences.getInstance();
  String? cartao = (prefs.getString("appControle"));
    await Future.delayed(const Duration(seconds: 1));
    if(cartao == 'controlcor'){
      return cartao;
    }
    else{
      return null;
    }
  }

  @override
  _AbsState createState() => _AbsState();
}

class _AbsState extends State<Abs> { 
  

  Future<void> osignUrl() async {
    if (!mounted) return;

    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges changes) {
      print("EMAIL SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setSMSSubscriptionObserver(
        (OSSMSSubscriptionStateChanges changes) {
      print("SMS SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    await OneSignal.shared
        .setAppId("f23a7bb4-e910-4230-8b8c-530d50587cb7");

    await OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);

    // Some examples of how to use In App Messaging public methods with OneSignal SDK
    oneSignalInAppMessagingTriggerExamples();

    OneSignal.shared.disablePush(false);

    // Some examples of how to use Outcome Events public methods with OneSignal SDK
    oneSignalOutcomeEventsExamples();

    bool userProvidedPrivacyConsent = await OneSignal.shared.userProvidedPrivacyConsent();
    print("USER PROVIDED PRIVACY CONSENT: $userProvidedPrivacyConsent");
  }

  oneSignalInAppMessagingTriggerExamples() async {
    /// Example addTrigger call for IAM
    /// This will add 1 trigger so if there are any IAM satisfying it, it
    /// will be shown to the user
    OneSignal.shared.addTrigger("trigger_1", "one");

    /// Example addTriggers call for IAM
    /// This will add 2 triggers so if there are any IAM satisfying these, they
    /// will be shown to the user
    Map<String, Object> triggers = <String, Object>{};
    triggers["trigger_2"] = "two";
    triggers["trigger_3"] = "three";
    OneSignal.shared.addTriggers(triggers);

    // Removes a trigger by its key so if any future IAM are pulled with
    // these triggers they will not be shown until the trigger is added back
    OneSignal.shared.removeTriggerForKey("trigger_2");

    // Get the value for a trigger by its key
    Object? triggerValue = await OneSignal.shared.getTriggerValueForKey("trigger_3");
    print("'trigger_3' key trigger value: ${triggerValue?.toString()}");

    // Create a list and bulk remove triggers based on keys supplied
    List<String> keys = ["trigger_1", "trigger_3"];
    OneSignal.shared.removeTriggersForKeys(keys);

    // Toggle pausing (displaying or not) of IAMs
    OneSignal.shared.pauseInAppMessages(false);
  }

  oneSignalOutcomeEventsExamples() async {
    // Await example for sending outcomes
    outcomeAwaitExample();

    // Send a normal outcome and get a reply with the name of the outcome
    OneSignal.shared.sendOutcome("normal_1");
    OneSignal.shared.sendOutcome("normal_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send a unique outcome and get a reply with the name of the outcome
    OneSignal.shared.sendUniqueOutcome("unique_1");
    OneSignal.shared.sendUniqueOutcome("unique_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send an outcome with a value and get a reply with the name of the outcome
    OneSignal.shared.sendOutcomeWithValue("value_1", 3.2);
    OneSignal.shared.sendOutcomeWithValue("value_2", 3.9).then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });
  } 

  Future<void> outcomeAwaitExample() async {
      var outcomeEvent = await OneSignal.shared.sendOutcome("await_normal_1");
      print(outcomeEvent.jsonRepresentation());
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    super.initState();
    osignUrl();
    setState(() {
      isM = true;
      isB2cor = true;
      isLoading = true;
    });
    //if(Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  clearUsr() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () 
    async {
      if (_webViewController != null) {
        _webViewController.goBack();
      }
      return true;
    },
    child: Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            isB2cor ?
            Center(
              child:FutureBuilder(
                future: Abs._urlb,
                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                ? Stack(
                  children: [
                    InAppWebView(
                      initialOptions: InAppWebViewGroupOptions(
                        android: AndroidInAppWebViewOptions(
                          useHybridComposition: true,
                          useOnRenderProcessGone: true,
                          useShouldInterceptRequest: true,
                          useWideViewPort: true,
                          supportMultipleWindows: true,
                        ),
                        crossPlatform: InAppWebViewOptions(
                          // set this to true if you are using window.open to open a new window with JavaScript
                          javaScriptCanOpenWindowsAutomatically: true
                        ),
                      ),
                      onWebViewCreated: (InAppWebViewController controller){
                        _webViewController = controller;
                        setState(() {
                          isLoading = true;
                        });
                      },
                      /*onCreateWindow: (controller, createWindowRequest) async {

                        print("onCreateWindow");

                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 400,
                                child: InAppWebView(
                                  initialOptions: InAppWebViewGroupOptions(
                                    android: AndroidInAppWebViewOptions(
                                      useHybridComposition: true,
                                      useOnRenderProcessGone: true,
                                      useShouldInterceptRequest: true,
                                      useWideViewPort: true,
                                      supportMultipleWindows: true,
                                    ),
                                    crossPlatform: InAppWebViewOptions(
                                      // set this to true if you are using window.open to open a new window with JavaScript
                                      javaScriptCanOpenWindowsAutomatically: true
                                    ),
                                  ),
                                  // Setting the windowId property is important here!
                                  windowId: createWindowRequest.windowId,
                                  onWebViewCreated: (InAppWebViewController controller) {
                                    _webViewPopupController = controller;
                                  },
                                  onLoadStart: (InAppWebViewController controller, Uri? url) {
                                    print("onLoadStart popup $url");
                                  },
                                  onLoadStop: (InAppWebViewController controller, Uri? url) {
                                    print("onLoadStop popup $url");
                                  },
                                  onLoadError: (InAppWebViewController controller, Uri? url, int i, String s) async {
                                    String url2 = url.toString();
                                    print('CUSTOM_HANDLER: $i, $s');
                                    String action = url2.split(':').first;
                                    List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                                    bool isCustomAction = customActions.contains(action);
                                    if (isCustomAction) {
                                      if (customActions.contains('whatsapp://send/')) {
                                        String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                                        await launch(
                                          ur3,
                                        );
                                      }
                                      else if (await canLaunch(url2)) {
                                        await launch(
                                          url2,
                                        );
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    } 
                                    if (isCustomAction) {
                                      if (customActions.contains('https://api.whatsapp.com/send/')) {
                                        String ur3 = url2.replaceAll('https://api.whatsapp.com/send/', 'https://wa.me/');
                                        await launch(
                                          ur3,
                                        );
                                      }
                                      else if (await canLaunch(url2)) {
                                        await launch(
                                          url2,
                                        );
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    }
                                  },
                                ),
                              ),
                            );
                          },
                        );

                        return true;
                      },*/
                      onLoadStart: (_controller, url){
                        setState(() {
                          isLoading = true;
                        });
                        if (url!.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/simp_antigo')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_analytics'
                          );          
                        }
                        else if (url.toString().contains('https://app.simp.agencialink.com/')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Menu()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp/'
                          );          
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tabelas'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_tradicional'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_moderno'
                          );  
                        }
                        else if (url.toString().contains('https://dashboard.simp.agencialink.com/acesso/profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                        else if (url.toString().contains('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional')) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs()));
                          launch(
                            'https://barra.agencialink.com.br/autologin/${snapshot.data}/simp_profissional'
                          );  
                        }
                      },
                      onLoadStop: (_controller, url){
                        setState(() {
                          isLoading = false;
                        });
                      },
                      initialUrlRequest:URLRequest(url: Uri.parse('https://barra.agencialink.com.br/autologin/${snapshot.data}/simp')),
                      onLoadError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action); 
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      },
                    ),
                    isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,))) : Stack()
                  ],
                )
                : Image.asset('imagens/2.gif', width: 150, height: 150,)),
            ) : Stack(),
            if (isM) Stack(
              children: [
                Center(child: Container(color: const Color.fromRGBO(0, 0, 0, 0.5),)),
                ClipRRect(
                borderRadius: const BorderRadius.only(bottomRight: Radius.circular(12), topRight: Radius.circular(12)),
                  child: Drawer(
                    child: Container(
                      color: Colors.white,
                      child: 
                      CustomScrollView(
                        slivers: [
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Padding(
                                  padding: const EdgeInsets.only(top: 30, bottom: 0, right: 80, left: 80),
                                  child: Image.asset('imagens/logo.png', width: 100, height: 100,),
                                ),
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                const Padding(
                                  padding: EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                Center(
                                  child: FutureBuilder(
                                    future: Abs._usrnm2,
                                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                    ? Column(
                                      children: [
                                        ExpandablePanel(
                                          header: 
                                          ListTile(
                                            leading: FutureBuilder(
                                              future: Abs._usrimg,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Stack(
                                                children: [
                                                  Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                                ],
                                              )
                                              : Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                            ),
                                            title: FutureBuilder(
                                              future: Abs._usrnm,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: const TextStyle(fontSize: 14),)
                                              : const CircularProgressIndicator()
                                            ),
                                            subtitle: FutureBuilder(
                                              future: Abs._uslog,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: const TextStyle(fontSize: 10),)
                                              : const CircularProgressIndicator()
                                            ),
                                          ),
                                          expanded: 
                                          ListTile(
                                            leading: FutureBuilder(
                                              future: Abs._usrimg2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Stack(
                                                children: [
                                                  Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                  Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                                ],
                                              )
                                              : Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                            ),
                                            title: FutureBuilder(
                                              future: Abs._usrnm2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: const TextStyle(fontSize: 14),)
                                              : const CircularProgressIndicator()
                                            ),
                                            subtitle: FutureBuilder(
                                              future: Abs._uslog2,
                                              builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                              ? Text(snapshot.data, style: const TextStyle(fontSize: 10),)
                                              : const CircularProgressIndicator()
                                            ),
                                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Abs2()));},
                                          ), 
                                          collapsed: const Center(),
                                        ),
                                        Center(
                                          child:
                                          TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: const Text('Trocar Usuário Extra'))
                                        )
                                      ],
                                    ) :
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        ListTile(
                                          leading: FutureBuilder(
                                            future: Abs._usrimg,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Stack(
                                              children: [
                                                Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                                Container(width: 60, height: 100, decoration: BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(snapshot.data,) )),),
                                              ],
                                            )
                                            : Container(width: 60, height: 100, decoration: const BoxDecoration(shape: BoxShape.circle, image: DecorationImage(fit: BoxFit.fill, image: AssetImage('imagens/ph.png',) )),),
                                          ),
                                          title: FutureBuilder(
                                            future: Abs._usrnm,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Text(snapshot.data, style: const TextStyle(fontSize: 14),)
                                            : const CircularProgressIndicator()
                                          ),
                                          subtitle: FutureBuilder(
                                            future: Abs._uslog,
                                            builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                            ? Text(snapshot.data, style: const TextStyle(fontSize: 10),)
                                            : const CircularProgressIndicator()
                                          ),
                                        ),
                                        Center(
                                          child:
                                          TextButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const AddUsr2(title: '',)));}, child: const Text('Adicionar Usuário Extra'))
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                const Padding(
                                  padding: EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverGrid(
                            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3), 
                            delegate: SliverChildListDelegate(
                              [
                                //_simp
                                FutureBuilder(
                                future: Abs._simp,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){
                                      setState(() {
                                        isM = false;
                                        isB2cor = true;
                                      });
                                    },
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/simp.png', width: 50, height: 50,),
                                        Text('SIMP', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/simpoff.png', width: 50, height: 50,),
                                        Text('SIMP', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_b2cor
                                FutureBuilder(
                                future: Abs._b2cor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const B2cor()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/b2cor.png', width: 50, height: 50,),
                                        Text('B2Cor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/b2coroff.png', width: 50, height: 50,),
                                        Text('B2Cor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_cardcor
                                FutureBuilder(
                                future: Abs._cdCor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const CardCor(title: '',)));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/cardcor.png', width: 50, height: 50,),
                                        Text('CardCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/cardcoroff.png', width: 50, height: 50,),
                                        Text('CardCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_mktcor
                                FutureBuilder(
                                future: Abs._mktcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Mktcor()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/mktcor.png', width: 50, height: 50,),
                                        Text('MktCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/mktcoroff.png', width: 50, height: 50,),
                                        Text('MktCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ), 
                                //_appcor
                                FutureBuilder(
                                future: Abs._appcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Appcor()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/appcor.png', width: 50, height: 50,),
                                        Text('AppCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : TextButton(
                                    onPressed: null,
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/appcoroff.png', width: 50, height: 50,),
                                        Text('AppCor', style: TextStyle(color: Colors.grey[400]))
                                      ],
                                    )
                                  )
                                ),
                                //_sivcor
                                FutureBuilder(
                                future: Abs._sivcor,
                                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                ? TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Sivcor()));},
                                    child: 
                                    Column(
                                      children: [
                                        Image.asset('imagens/ferramentas/sivcor.png', width: 50, height: 50,),
                                        Text('SivCor', style: TextStyle(color: Colors.grey[700]))
                                      ],
                                    )
                                  )
                                : //_controlcor
                                  FutureBuilder(
                                  future: Abs._controlcor,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                  ? TextButton(
                                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Controlcor()));},
                                      child: 
                                      Column(
                                        children: [
                                          Image.asset('imagens/ferramentas/controlcor.png', width: 50, height: 50,),
                                          Text('ControlCor', style: TextStyle(color: Colors.grey[700]))
                                        ],
                                      )
                                    )
                                  : //_sivcorff
                                    FutureBuilder(
                                    future: Abs._appControle,
                                    builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                    ? TextButton(
                                        onPressed: null,
                                        child: 
                                        Column(
                                          children: [
                                            Image.asset('imagens/ferramentas/sivcoroff.png', width: 50, height: 50,),
                                            Text('SivCor', style: TextStyle(color: Colors.grey[400]))
                                          ],
                                        )
                                      )
                                    : //_controlcoroff
                                      FutureBuilder(
                                      future: Abs._appControle2,
                                      builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                                      ? TextButton(
                                          onPressed: null,
                                          child: 
                                          Column(
                                            children: [
                                              Image.asset('imagens/ferramentas/controlcoroff.png', width: 50, height: 50,),
                                              Text('ControlCor', style: TextStyle(color: Colors.grey[400]))
                                            ],
                                          )
                                        )
                                      : const Visibility(child: Text(""), visible: false,)
                                      ),
                                    ), 
                                  ),
                                ), 
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                const Padding(
                                  padding: EdgeInsets.only(left: 15,right: 15),
                                  child: Divider(),
                                ),   
                              ]
                            )
                          ),
                          SliverList(
                            delegate: 
                            SliverChildListDelegate(
                              [
                                ListTile(
                                  leading: Image.asset('imagens/sup.png', width: 35, height: 35,),
                                  title: const Text('Suporte - Base de Conhecimento', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Ajuda()));}, 
                                ),
                                ListTile(
                                  leading: Image.asset('imagens/uni.png', width: 35, height: 35,),
                                  title: const Text('Universidade Digital', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Treinacor()));}, 
                                ),
                                ListTile(
                                  leading: Image.asset('imagens/cli.png', width: 35, height: 35,),
                                  title: const Text('Painel do Cliente', style: TextStyle(fontSize: 12), textAlign: TextAlign.left,),
                                  onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Painel()));}, 
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: 
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Image.asset('imagens/ag.png', width: 102, height: 50,)
                                    ],
                                  ),
                                ),
                              ]
                            )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ) else Stack(),
          ],
        ),
      ),
      bottomNavigationBar: 
      ConvexAppBar(
        curveSize: 100,
        top: -15,
        style: TabStyle.fixedCircle,
        backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
        items: 
        [
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.home, color: Colors.white,), 
              onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Simp()));}, 
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goBack();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.apps, color: Colors.white,),
              onPressed: (){
                setState(() 
                  {
                    isM = true;
                    isB2cor = true;
                  }
                );
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goForward();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
              onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
            )
          ),
        ],
      )
    ),
  ); 
}
/*
class WebViewWidget extends StatefulWidget {
  final String url;
  WebViewWidget({this.url});

  @override
  _WebViewWidget createState() => _WebViewWidget();
}

class _WebViewWidget extends State<WebViewWidget> {
  WebView _webView;
  @override
  void initState() {
    super.initState();
     _webView = WebView(
       key: _key,
      initialUrl: widget.url,
      javascriptMode: JavascriptMode.unrestricted,
      onPageFinished: (finish){
        setState(() {
          isLoading = false;
        });
      },
      onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
      },
      navigationDelegate: (NavigationRequest request)  {
        if (request.url.startsWith("mailto:")) {
          launch(request.url);
          return NavigationDecision.prevent;
        } else if (request.url.startsWith("whatsapp:")) {
          launch(request.url);
          return NavigationDecision.prevent;
        } else if (request.url.startsWith("fb-messenger:")) {
          launch(request.url);
          return NavigationDecision.prevent;
        } else if (request.url.startsWith("tg:")) {
          launch(request.url);
          return NavigationDecision.prevent;
        } else {
          return NavigationDecision.navigate;
        }
      },
      gestureNavigationEnabled: true,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _webView = null;
  }

  @override
  Widget build(BuildContext context) => _webView;
}*/