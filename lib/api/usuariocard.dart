class UsuarioCard {
  late String nome;
  late String fone;
  late String responsavel; 
 
  UsuarioCard({ nome,  fone,  responsavel});

  UsuarioCard.fromJson(Map<String, dynamic> json) {
    nome = json['nome'];
    fone = json['fone'];
    responsavel = json['responsavel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['nome'] =  nome;
    data['fone'] =  fone;
    data['responsavel'] =  responsavel;
    return data;
  }

  @override
  String toString() {
    return 'Usuario(nome: $nome)';
  }
  
  String toInt() {
    return responsavel;
  }
}