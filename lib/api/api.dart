// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/usuarionew.dart';
import '../api/home_block.dart';

class LoginApi{
 
  var bloc = BlocHome();

  void initState(){
    bloc.initOneSignal();
  }  

  static Future<Usuario?> login(String login, String senha, String onesignalUserId, int permis) async {
    var status = await OneSignal.shared.getDeviceState();

    var url = Uri.parse('https://b2corapi.agencialink.com.br/login/');
    var header = {"Content-Type" : "application/json"};

    String? onesignalUserId = status?.userId;

    Map params = {
      "login" : login,
      "senha" : senha,
      "onesignal_id" : onesignalUserId,
      "detalhes" : permis,
    };

    var usuario;

    var prefs = await SharedPreferences.getInstance();

    if(onesignalUserId!.isNotEmpty){
      prefs.setString("oneid", onesignalUserId);
    }

    var _body = json.encode(params);  
    print("json enviado : $_body"); 

    var response = await http.post(url, headers: header, body: _body);
    print('Response status: ${response.statusCode}'); 

    Map<String, dynamic> mapResponse = json.decode(response.body);

    if(mapResponse["code"] == json.decode('12')){
      //usuario = Usuario.fromJson(mapResponse); 
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
      print('Resposta $mapResponse');
      }
      else if(mapResponse["status"] == json.decode('false')){
      //usuario = Usuario.fromJson(mapResponse);
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
      print('Resposta $mapResponse');
      }
      else if(mapResponse["logotipo_usuario"] == false){        
        usuario = Usuario.fromJson(mapResponse);
        //bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt", mapResponse["token"]);
        prefs.setString("usr_tkn", mapResponse["user_token"]);
        prefs.setString("usr_pass", mapResponse["user_pass"]);
        prefs.setString("nome", mapResponse["name"]);
        prefs.setString("login", mapResponse["login"]);
        prefs.setString("id", mapResponse["id"]);
        //prefs.setBool("treinacor", treinacor);
        prefs.setBool("controlcor", controlcor);
        prefs.setBool("sivcor", sivcor);
        prefs.setBool("appcor", appcor);
        prefs.setBool("cardcor", cardcor);
        prefs.setBool("b2cor", b2cor);
        prefs.setBool("pontodigital", pontodigital);
        prefs.setBool("mktcor", mktcor);
        prefs.setBool("simp", simp);
        prefs.setString("appControle", appControle);
        if (mapResponse["cartao"] != false || mapResponse["cartao"] != null) {
          try {
            prefs.setString("cartao", mapResponse["cartao"]);
          } catch (e) {
            prefs.setBool("cartao21", mapResponse["cartao"]);
          }
        }
        print('Respostagmfgmf $appcor');
        print('Resposta $mapResponse');
      }
      else if(mapResponse["cartao"] == false){        
        usuario = Usuario.fromJson(mapResponse);
        bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt", mapResponse["token"]);
        prefs.setString("usr_tkn", mapResponse["user_token"]);
        prefs.setString("usr_pass", mapResponse["user_pass"]);
        prefs.setString("nome", mapResponse["name"]);
        prefs.setString("login", mapResponse["login"]);
        prefs.setString("logotipousuario", mapResponse["logotipo_usuario"]);
        prefs.setString("id", mapResponse["id"]);
        prefs.setBool("treinacor", treinacor);
        prefs.setBool("controlcor", controlcor); 
        prefs.setBool("sivcor", sivcor);
        prefs.setBool("appcor", appcor);
        prefs.setBool("cardcor", cardcor);
        prefs.setBool("b2cor", b2cor);
        prefs.setBool("pontodigital", pontodigital);
        prefs.setBool("mktcor", mktcor);
        prefs.setBool("simp", simp);
        prefs.setString("appControle", appControle);
        if (mapResponse["cartao"] != false || mapResponse["cartao"] != null) {
          try {
            prefs.setString("cartao", mapResponse["cartao"]);
          } catch (e) {
            prefs.setBool("cartao21", mapResponse["cartao"]);
          }
        }
        print('Respostagmfgmf $appcor');
        print('Resposta $mapResponse');
      }
      else if(response.statusCode == 200){        
        usuario = Usuario.fromJson(mapResponse);
        bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt", mapResponse["token"]);
        prefs.setString("usr_tkn", mapResponse["user_token"]);
        prefs.setString("usr_pass", mapResponse["user_pass"]);
        prefs.setString("nome", mapResponse["name"]);
        prefs.setString("login", mapResponse["login"]);
        prefs.setString("logotipousuario", mapResponse["logotipo_usuario"]);
        prefs.setString("cartao", mapResponse["cartao"]);
        prefs.setString("id", mapResponse["id"]);
        prefs.setBool("treinacor", treinacor);
        prefs.setBool("controlcor", controlcor);
        prefs.setBool("sivcor", sivcor);
        prefs.setBool("appcor", appcor);
        prefs.setBool("cardcor", cardcor);
        prefs.setBool("b2cor", b2cor);
        prefs.setBool("pontodigital", pontodigital);
        prefs.setBool("mktcor", mktcor);
        prefs.setBool("simp", simp);
        prefs.setString("appControle", appControle);
        print('Respostagmfgmf $appcor');
        print('Resposta $mapResponse');
      }
      return usuario;
    }
}