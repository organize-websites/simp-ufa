// ignore_for_file: prefer_void_to_null

class Usuario {
  late String id;
  late String login;
  late String name;
  late String token;
  late String userToken;
  late String userPass;
  late String logotipo;
  late Null email;
  late Permissoes permissoes;

  Usuario(
      { id,
       login,
       name,
       token,
       userToken,
       userPass,
       logotipo,
       email,
       permissoes});

  Usuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    login = json['login'];
    name = json['name'];
    token = json['token'];
    userToken = json['user_token'];
    userPass = json['user_pass'];
    logotipo = json['logotipo'];
    email = json['email'];
    permissoes = Permissoes.fromJson(json['permissoes']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] =  id;
    data['login'] =  login;
    data['name'] =  name;
    data['token'] =  token;
    data['user_token'] =  userToken;
    data['user_pass'] =  userPass;
    data['logotipo'] =  logotipo;
    data['email'] =  email;
    data['permissoes'] =  permissoes.toJson();
    return data;
  }
}

class Telefones {
  late String id;
  late String idCliente;
  late String ddd;
  late String fone;
  late String operadora;
  late String ordem;
  late String whatsapp;
  late String tipo;
  late String dataCad;
  late String ativo;
  late String confirmado;

  Telefones(
      { id,
       idCliente,
       ddd,
       fone,
       operadora,
       ordem,
       whatsapp,
       tipo,
       dataCad,
       ativo,
       confirmado});

  Telefones.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idCliente = json['id_cliente'];
    ddd = json['ddd'];
    fone = json['fone'];
    operadora = json['operadora'];
    ordem = json['ordem'];
    whatsapp = json['whatsapp'];
    tipo = json['tipo'];
    dataCad = json['data_cad'];
    ativo = json['ativo'];
    confirmado = json['confirmado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] =  id;
    data['id_cliente'] =  idCliente;
    data['ddd'] =  ddd;
    data['fone'] =  fone;
    data['operadora'] =  operadora;
    data['ordem'] =  ordem;
    data['whatsapp'] =  whatsapp;
    data['tipo'] =  tipo;
    data['data_cad'] =  dataCad;
    data['ativo'] =  ativo;
    data['confirmado'] =  confirmado;
    return data;
  }
}

class Redes {
  late String site;
  late String facebook;
  late String youtube;
  late String twitter;
  late String instagram;
  late String linkedin;
  late String blogspot;

  Redes(
      { site,
       facebook,
       youtube,
       twitter,
       instagram,
       linkedin,
       blogspot});

  Redes.fromJson(Map<String, dynamic> json) {
    site = json['site'];
    facebook = json['facebook'];
    youtube = json['youtube'];
    twitter = json['twitter'];
    instagram = json['instagram'];
    linkedin = json['linkedin'];
    blogspot = json['blogspot'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['site'] =  site;
    data['facebook'] =  facebook;
    data['youtube'] =  youtube;
    data['twitter'] =  twitter;
    data['instagram'] =  instagram;
    data['linkedin'] =  linkedin;
    data['blogspot'] =  blogspot;
    return data;
  }
}

class Websocket {
  late String b2corUserToken;
  late String url;

  Websocket({ b2corUserToken,  url});

  Websocket.fromJson(Map<String, dynamic> json) {
    b2corUserToken = json['b2corUserToken'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['b2corUserToken'] =  b2corUserToken;
    data['url'] =  url;
    return data;
  }
}

class Permissoes {
  late bool treinacor;
  late bool controlcor;
  late bool sivcor;
  late bool appcor;
  late bool cardcor;
  late bool b2cor;
  late bool pontodigital;
  late bool mktcor;
  late bool simp;
  late bool simpTabelas;
  late bool simpTradicional;
  late bool simpModerno;
  late bool simpProfissional;
  late bool fem;
  late String appControle;

  Permissoes(
      { treinacor,
       controlcor,
       sivcor,
       appcor,
       cardcor,
       b2cor,
       pontodigital,
       mktcor,
       simp,
       simpTabelas,
       simpTradicional,
       simpModerno,
       simpProfissional,
       fem,
       appControle});

  Permissoes.fromJson(Map<String, dynamic> json) {
    treinacor = json['treinacor'];
    controlcor = json['controlcor'];
    sivcor = json['sivcor'];
    appcor = json['appcor'];
    cardcor = json['cardcor'];
    b2cor = json['b2cor'];
    pontodigital = json['pontodigital'];
    mktcor = json['mktcor'];
    simp = json['simp'];
    simpTabelas = json['simp_tabelas'];
    simpTradicional = json['simp_tradicional'];
    simpModerno = json['simp_moderno'];
    simpProfissional = json['simp_profissional'];
    fem = json['fem'];
    appControle = json['app_controle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['treinacor'] =  treinacor;
    data['controlcor'] =  controlcor;
    data['sivcor'] =  sivcor;
    data['appcor'] =  appcor;
    data['cardcor'] =  cardcor;
    data['b2cor'] =  b2cor;
    data['pontodigital'] =  pontodigital;
    data['mktcor'] =  mktcor;
    data['simp'] =  simp;
    data['simp_tabelas'] =  simpTabelas;
    data['simp_tradicional'] =  simpTradicional;
    data['simp_moderno'] =  simpModerno;
    data['simp_profissional'] =  simpProfissional;
    data['fem'] =  fem;
    data['app_controle'] =  appControle;
    return data;
  }
}