class UsuarioCard2 {
  late String nome2;
  late String fone2;
  late String responsavel2;
 
  UsuarioCard2({ nome2,  fone2,  responsavel2});

  UsuarioCard2.fromJson(Map<String, dynamic> json) {
    nome2 = json['nome'];
    fone2 = json['fone'];
    responsavel2 = json['responsavel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['nome'] =  nome2;
    data['fone'] =  fone2;
    data['responsavel'] =  responsavel2;
    return data;
  }

  @override
  String toString() {
    return 'Usuario(nome: $nome2)';
  }
  
  String toInt() {
    return responsavel2;
  }
}