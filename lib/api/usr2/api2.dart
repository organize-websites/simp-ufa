// ignore_for_file: prefer_typing_uninitialized_variables, avoid_print

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../home_block.dart';
import '../usuarionew.dart';

class LoginApi2{
  
  var bloc = BlocHome();

  void initState(){
    bloc.initOneSignal();  
  }  

  static Future<Usuario?> login2(String login, String senha, String onesignalUserId, int permis) async {
    var status = await OneSignal.shared.getDeviceState();

    var url = Uri.parse('https://b2corapi.agencialink.com.br/login/');
    var header = {"Content-Type" : "application/json"};

    String? onesignalUserId = status?.userId;

    Map params = {
      "login" : login,
      "senha" : senha,
      "onesignal_id" : onesignalUserId,
      "detalhes" : permis,
    };

    var usuario;

    var prefs = await SharedPreferences.getInstance();

    if(onesignalUserId!.isNotEmpty){
      prefs.setString("oneid2", onesignalUserId);
    }

    var _body = json.encode(params);  
    print("json enviado : $_body"); 

    var response = await http.post(url, headers: header, body: _body);
    print('Response status: ${response.statusCode}');

    Map<String, dynamic> mapResponse = json.decode(response.body);

    if(mapResponse["code"] == json.decode('12')){
      //usuario = Usuario.fromJson(mapResponse);
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
      }
      else if(mapResponse["status"] == json.decode('false')){
      //usuario = Usuario.fromJson(mapResponse);
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
      }
      else if(mapResponse["logotipo_usuario"] == false){        
        usuario = Usuario.fromJson(mapResponse);
        //bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt2", mapResponse["token"]);
        prefs.setString("usr_tkn2", mapResponse["user_token"]);
        prefs.setString("usr_pass2", mapResponse["user_pass"]);
        prefs.setString("nome2", mapResponse["name"]);
        prefs.setString("login2", mapResponse["login"]);
        prefs.setString("id2", mapResponse["id"]);
        //prefs.setBool("treinacor2", treinacor);
        prefs.setBool("controlcor2", controlcor);
        prefs.setBool("sivcor2", sivcor);
        prefs.setBool("appcor2", appcor);
        prefs.setBool("cardcor2", cardcor);
        prefs.setBool("b2cor2", b2cor);
        prefs.setBool("pontodigital2", pontodigital);
        prefs.setBool("mktcor2", mktcor);
        prefs.setBool("simp2", simp);
        prefs.setString("appControle2", appControle);
      }
      else if(mapResponse["cartao"] == false){        
        usuario = Usuario.fromJson(mapResponse);
        //bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt2", mapResponse["token"]);
        prefs.setString("usr_tkn2", mapResponse["user_token"]);
        prefs.setString("usr_pass2", mapResponse["user_pass"]);
        prefs.setString("nome2", mapResponse["name"]);
        prefs.setString("login2", mapResponse["login"]);
        prefs.setString("logotipousuario2", mapResponse["logotipo_usuario"]);
        prefs.setString("id2", mapResponse["id"]);
        //prefs.setBool("treinacor2", treinacor);
        prefs.setBool("controlcor2", controlcor);
        prefs.setBool("sivcor2", sivcor);
        prefs.setBool("appcor2", appcor);
        prefs.setBool("cardcor2", cardcor);
        prefs.setBool("b2cor2", b2cor);
        prefs.setBool("pontodigital2", pontodigital);
        prefs.setBool("mktcor2", mktcor);
        prefs.setBool("simp2", simp);
        prefs.setString("appControle2", appControle);
      }
      else if(response.statusCode == 200){        
        usuario = Usuario.fromJson(mapResponse);
        //bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
        bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
        bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
        bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor; 
        bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
        bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
        bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
        bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
        bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
        String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
        prefs.setString("tokenjwt2", mapResponse["token"]);
        prefs.setString("usr_tkn2", mapResponse["user_token"]);
        prefs.setString("usr_pass2", mapResponse["user_pass"]);
        prefs.setString("nome2", mapResponse["name"]);
        prefs.setString("login2", mapResponse["login"]);
        prefs.setString("logotipousuario2", mapResponse["logotipo_usuario"]);
        prefs.setString("cartao2", mapResponse["cartao"]);
        prefs.setString("id2", mapResponse["id"]);
        //prefs.setBool("treinacor2", treinacor);
        prefs.setBool("controlcor2", controlcor);
        prefs.setBool("sivcor2", sivcor);
        prefs.setBool("appcor2", appcor);
        prefs.setBool("cardcor2", cardcor);
        prefs.setBool("b2cor2", b2cor);
        prefs.setBool("pontodigital2", pontodigital);
        prefs.setBool("mktcor2", mktcor);
        prefs.setBool("simp2", simp);
        prefs.setString("appControle2", appControle);
      }
      return usuario;
    }
}