// ignore_for_file: avoid_print, unused_element, prefer_const_constructors, unused_field

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'absoluta.dart';
import 'cadastro.dart';
import 'privacidade.dart';
import 'api/home_block.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

import 'alerta.dart';
import 'api/api.dart';
import 'esqueci_a_senha.dart';

const androidString = AndroidAuthMessages(
  cancelButton: 'Cancelar',
  signInTitle: 'Autenticação Biometrica',
  biometricHint: 'Use sua Digital',
  biometricNotRecognized: 'Não confere, tente novamente',
  biometricSuccess: 'Sucesso! Aguarde...',
  goToSettingsButton: 'Configurações',
  goToSettingsDescription: 'Você precisa usar a biometria',
  biometricRequiredTitle: 'Biometria Necessaria'
);

const iosString = IOSAuthMessages(
  lockOut: 'Você precisa habilitar uma biometria',
  goToSettingsButton: 'Configurações',
  goToSettingsDescription: 'Configure uma biometria',
  cancelButton: 'Cancelar'
);
 

Future<void> main() async {
  var prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString('tokenjwt');
  print(token);
  runApp(MaterialApp(home: token == null ? const HomePage(title: '',) : Abs()));
}

bool isbs = true;

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
 
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  LocalAuthentication auth = LocalAuthentication();
  late bool _canCheckBiometric;
  String authorized = "Not authorized";

  // verificação bimétrica
  // esta função irá verificar os sensores e nos informará
  // se podemos usá-los ou não
  Future<void> _checkBiometric() async{
    late bool canCheckBiometric;
    try{
      canCheckBiometric = await auth.canCheckBiometrics;
    } on PlatformException catch(e){
      print(e);
    }
    if(!mounted) return;
    setState(() {
      _canCheckBiometric = canCheckBiometric;
    });
  }

  // esta função abrirá uma caixa de diálogo de autenticação
  // e verificará se estamos autenticados ou não
  // então adicionaremos a ação principal aqui, como mudar para outra atividade
  // ou apenas exibir um texto que nos diga que estamos autenticados
  Future<void> _authenticate() async{
    bool authenticated = false;
    try{
      authenticated = await auth.authenticate(
          localizedReason: "Use a biometria para autenticar",
          useErrorDialogs: false,
          androidAuthStrings: androidString,
          iOSAuthStrings: iosString,
          stickyAuth: true,
          biometricOnly: true
      );
    } on PlatformException catch(e){
      if (e.code == auth_error.notEnrolled) {
        usrauth();
      }
      if (e.code == auth_error.notAvailable){
        usrauth();
      }
      if (e.code == auth_error.passcodeNotSet){
        usrauth();
      }
      print(e);
    }
    if(!mounted) return;
    setState(() {
       authorized = authenticated ? usrauth() : "Falha ao autenticar";
    });
  }

  usrauth(){
    _onClickLogin(context); _doSomething();
  }

  var bloc = BlocHome();

  paraOutraTela() async{
    var _duration = const Duration(seconds: 1);
    return Timer(_duration, main);
  }

  Future<void> osignUrl() async {
    if (!mounted) return;

    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges changes) {
      print("EMAIL SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setSMSSubscriptionObserver(
        (OSSMSSubscriptionStateChanges changes) {
      print("SMS SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    await OneSignal.shared
        .setAppId("f23a7bb4-e910-4230-8b8c-530d50587cb7");

    await OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);

    // Some examples of how to use In App Messaging public methods with OneSignal SDK
    oneSignalInAppMessagingTriggerExamples();

    OneSignal.shared.disablePush(false);

    // Some examples of how to use Outcome Events public methods with OneSignal SDK
    oneSignalOutcomeEventsExamples();

    bool userProvidedPrivacyConsent = await OneSignal.shared.userProvidedPrivacyConsent();
    print("USER PROVIDED PRIVACY CONSENT: $userProvidedPrivacyConsent");
  }

  oneSignalInAppMessagingTriggerExamples() async {
    /// Example addTrigger call for IAM
    /// This will add 1 trigger so if there are any IAM satisfying it, it
    /// will be shown to the user
    OneSignal.shared.addTrigger("trigger_1", "one");

    /// Example addTriggers call for IAM
    /// This will add 2 triggers so if there are any IAM satisfying these, they
    /// will be shown to the user
    Map<String, Object> triggers = <String, Object>{};
    triggers["trigger_2"] = "two";
    triggers["trigger_3"] = "three";
    OneSignal.shared.addTriggers(triggers);

    // Removes a trigger by its key so if any future IAM are pulled with
    // these triggers they will not be shown until the trigger is added back
    OneSignal.shared.removeTriggerForKey("trigger_2");

    // Get the value for a trigger by its key
    Object? triggerValue = await OneSignal.shared.getTriggerValueForKey("trigger_3");
    print("'trigger_3' key trigger value: ${triggerValue?.toString()}");

    // Create a list and bulk remove triggers based on keys supplied
    List<String> keys = ["trigger_1", "trigger_3"];
    OneSignal.shared.removeTriggersForKeys(keys);

    // Toggle pausing (displaying or not) of IAMs
    OneSignal.shared.pauseInAppMessages(false);
  }

  oneSignalOutcomeEventsExamples() async {
    // Await example for sending outcomes
    outcomeAwaitExample();

    // Send a normal outcome and get a reply with the name of the outcome
    OneSignal.shared.sendOutcome("normal_1");
    OneSignal.shared.sendOutcome("normal_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send a unique outcome and get a reply with the name of the outcome
    OneSignal.shared.sendUniqueOutcome("unique_1");
    OneSignal.shared.sendUniqueOutcome("unique_2").then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });

    // Send an outcome with a value and get a reply with the name of the outcome
    OneSignal.shared.sendOutcomeWithValue("value_1", 3.2);
    OneSignal.shared.sendOutcomeWithValue("value_2", 3.9).then((outcomeEvent) {
      print(outcomeEvent.jsonRepresentation());
    });
  } 

  Future<void> outcomeAwaitExample() async {
      var outcomeEvent = await OneSignal.shared.sendOutcome("await_normal_1");
      print(outcomeEvent.jsonRepresentation());
  }

  @override
  initState() {
    super.initState();
    osignUrl();
    paraOutraTela();
    setState(() {
      isbs = true;
    });
    //_checkBiometric();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
  }


  final RoundedLoadingButtonController _btnController = RoundedLoadingButtonController();

  void _doSomething() async {
    Timer(const Duration(seconds: 3), () {
      _btnController.reset();
    });
  }

static Future<String?> get _usrid async {

  var prefs = await SharedPreferences.getInstance();
  String oneid = (prefs.getString("oneid") ?? "");

String toString() {
    return oneid;
  }

    await Future.delayed(const Duration(seconds: 1));
    return oneid;
  } 
  
  final _tLogin = TextEditingController();

  final _tSenha = TextEditingController();

  final _tonesignalUserId = _usrid;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SIMP Simuladores Online',
      theme: ThemeData.light(),
      home: Scaffold(
        body: 
        Container(
          decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('imagens/bg.png'), fit: BoxFit.cover,)),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 10, left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    children: [
                      Column(
                        verticalDirection: VerticalDirection.up,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 25),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: const [
                                Text('É necessário ter uma conta para acessar...', style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
                                //const Text('Não possuí login e senha', style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
                                //TextButton(child: const Text('Faça seu cadastro aqui!', style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,), onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Cadastro()));},)
                              ]
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10, right: 10, left: 10),
                            child: Container(
                              height: 500,
                              width: 400,                              
                              decoration: const BoxDecoration(image: DecorationImage(image: AssetImage('imagens/bg-f.png'), fit: BoxFit.fitWidth,)),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 110),
                                child:
                                _body(context),
                              )
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ]
              ),
            ),
          ),
        ),
      ),
    );
  }



  _body(BuildContext context) {
  return Form(
    key: _formKey,
    child: Padding(
      padding: const EdgeInsets.only(top: 30, bottom: 40, left: 25, right: 25),
      child: ListView(
        children: <Widget>[
          textFormFieldLogin(),
          textFormFieldSenha(),
          containerButton(context),
          Row(
            verticalDirection: VerticalDirection.up,
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>Privacidade()));}, child: Text('Privacidade', style: TextStyle(color: Colors.grey[600]),)),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>Esqueci()));}, child: Text('? Lembrar Senha', style: TextStyle(color: Colors.grey[600]),)),
                ],
              ),
            ],
          )
        ],
      ),
    )
  );
}
 
textFormFieldLogin() {
  return Padding(
    padding: const EdgeInsets.only(top: 55),
    child: TextFormField(
      controller: _tLogin,
             validator: _validateLogin,
      keyboardType: TextInputType.text,
      style: TextStyle(color: Colors.grey[600]),
      decoration: InputDecoration( 
        labelText: "Login",
        labelStyle: TextStyle(color: Colors.grey[600]),
        hintText: "Informe o login"
      )
    ),
  );
}

textFormFieldSenha() {
  return TextFormField( 
    controller: _tSenha,
            validator: _validateSenha,
    obscureText: isbs,
    keyboardType: TextInputType.text,
    style: TextStyle(color: Colors.grey[600]),
    decoration: InputDecoration( 
      suffixIcon: IconButton(
        onPressed: (){isbs ? setState(() {
          isbs = false;
        }):
        setState(() {
          isbs = true;
        });}, 
        icon: FaIcon(
          isbs ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
          color: Colors.grey[600],)
      ),
      labelText: "Senha",
      labelStyle: TextStyle(color: Colors.grey[600]),
      hintText: "Informe a senha"
    )
  );
}

containerButton(BuildContext context) {
  return Container(
    height: 35.0,
    width: 90.0,
    margin: const EdgeInsets.only(top: 10.0, right: 55, left: 55),
    child: 
    RoundedLoadingButton(
      borderRadius: 5,
      color: Color.fromRGBO(52, 58, 64, 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text("Acessar  ", style: TextStyle(color: Colors.white, fontSize: 17.0)),
          Icon(Icons.arrow_forward, color: Colors.white,)
        ],
      ),
      controller: _btnController,
      onPressed: () {
        /*if(_canCheckBiometric == true){
          _authenticate();
        }
        else{*/
          _onClickLogin(context);
          _doSomething();
        //}
      },
      width: 200,
    ),
  );
}

  _onClickLogin(BuildContext context) async { 
    var prefs = await SharedPreferences.getInstance();
    final login = _tLogin.text;
    final senha = _tSenha.text;
    final onesignalUserId = _tonesignalUserId;
    print("Login: $login , Senha: $senha , os: $onesignalUserId" );      

    if( login.isEmpty || senha.isEmpty ){
      alert2(context, "Preencha os Dados");
     }
     else {
       var usuario = await LoginApi.login(login,senha,toString(),1);
       // ignore: unnecessary_null_comparison
       if( usuario != null ) {
          navegaHomeSimp(context);
          prefs.setString("login", login);
          prefs.setString("senha", senha);
        }
       else {
        String msg = (prefs.getString("msg") ?? "");
        alert(context, msg);
       }    
     }
  }

  navegaHomeSimp(BuildContext context){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>Abs()),);
  }

String? _validateLogin(String? text){
    if(text!.isEmpty){
      return alert(context, "Informe o login");
    }
    return null;
}

String? _validateSenha(String? text){
    if(text!.isEmpty){
      return "Informe a senha";
    }
    return null;
  } 

  
  @override
  void dispose() {
    super.dispose();
    paraOutraTela();
    _checkBiometric();
  }
}
