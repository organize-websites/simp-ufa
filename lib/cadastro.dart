// ignore_for_file: avoid_print, unused_field

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';



class Cadastro extends StatefulWidget {
  const Cadastro({Key? key}) : super(key: key);

  static Future<URLRequest> get _urlapp async { 
    await Future.delayed(const Duration(seconds: 1));
    return URLRequest(url: Uri.parse('https://simpsimuladores.online/cadastro?ass=73738'));
  }

  @override
  _CadastroState createState() => _CadastroState();
}

class _CadastroState extends State<Cadastro> {
  bool isLoading = true;

  late InAppWebViewController _webViewController;

  @override

      void initState() {
        setState(() {
          isLoading = true;
        });
        super.initState();
      }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
          return Center(
              child:FutureBuilder(
                future: Cadastro._urlapp,
                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                ? Stack(
                  children: [
                    InAppWebView( 
                      onWebViewCreated: (InAppWebViewController controller){
                        _webViewController = controller;
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onLoadStart: (_controller, url){
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onLoadStop: (_controller, url){
                        setState(() {
                          isLoading = false;
                        });
                      },
                      initialUrlRequest: snapshot.data,
                      onLoadError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      }
                    ),
                    isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,))) : Stack()
                  ],
                )
                : Image.asset('imagens/2.gif', width: 150, height: 150,)),
            );
        }
      ),
      bottomNavigationBar: 
      SizedBox(
      height: 60,
        child: BottomNavigationBar(
          backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
          items: [
            BottomNavigationBarItem(
              label: '',
              icon: IconButton( 
                icon: const Icon(Icons.arrow_back, color: Colors.white,), 
                onPressed: (){Navigator.pop(context);}
              ) 
            ),
            BottomNavigationBarItem(
              label: '',
              icon: IconButton(
                icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
                onPressed: (){exit(0);},
              ) 
            ),
          ],
        ),
      ),
    );
  }
}