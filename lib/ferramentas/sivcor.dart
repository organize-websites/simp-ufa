// ignore_for_file: avoid_print

import 'dart:async';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import '../ferramentas/menu.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../alerta.dart'; 

bool isLoading = true;

late InAppWebViewController _webViewController;

class Sivcor extends StatefulWidget {
  const Sivcor({Key? key}) : super(key: key);


  static Future<URLRequest> get _urlsiv async {

  var prefs2 = await SharedPreferences.getInstance();
  String usertoken = (prefs2.getString("usr_tkn") ?? "");


    await Future.delayed(const Duration(seconds: 1));
    return URLRequest(url: Uri.parse('https://barra.agencialink.com.br/autologin/$usertoken/sivcor'));
  }

  @override
  _SivcorState createState() => _SivcorState();
}

class _SivcorState extends State<Sivcor> { 

   

  navOutra() {
    setState(() {
      isLoading = false;
    });
  }

  paraOutraTela() async{
    var _duration = const Duration(seconds: 6);
    return Timer(_duration, navOutra);
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
     super.initState();
    setState(() {
          isLoading = true;
        });
    paraOutraTela();
  }

  clearUsr() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();  
  
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () 
    async{
      // ignore: unnecessary_null_comparison
      if (_webViewController != null) {
        _webViewController.goBack();
      }
      return true;
    },
    child: Scaffold(
      key: _scaffoldKey,
      body: 
      Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Center(
              child:FutureBuilder(
                future: Sivcor._urlsiv,
                builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                ? Stack(
                  children: [
                    InAppWebView( 
                      initialOptions: InAppWebViewGroupOptions(
                        android: AndroidInAppWebViewOptions(
                          useHybridComposition: true,
                          useOnRenderProcessGone: true,
                          useShouldInterceptRequest: true,
                          useWideViewPort: true,
                        ),
                      ),
                      onWebViewCreated: (InAppWebViewController controller){
                        _webViewController = controller;
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onLoadStart: (_controller, url){
                        setState(() {
                          isLoading = true;
                        });
                      },
                      onLoadStop: (_controller, url){
                        setState(() {
                          isLoading = false;
                        });
                      },
                      initialUrlRequest: snapshot.data,
                      onLoadHttpError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      },
                      onLoadError: (InAppWebViewController _controller, Uri? url, int i, String s) async {
                        _controller.goBack();
                        _controller.goBack();
                        String url2 = url.toString();
                        print('CUSTOM_HANDLER: $i, $s');
                        String action = url2.split(':').first;
                        List<String> customActions = ['tel', 'whatsapp', 'mailto'];
                        bool isCustomAction = customActions.contains(action);
                        if (isCustomAction) {
                          if (customActions.contains('whatsapp')) {
                            String ur3 = url2.replaceAll('whatsapp://send/', 'https://wa.me/');
                            await launch(
                              ur3,
                            );
                          }
                          else if (await canLaunch(url2)) {
                            await launch(
                              url2,
                            );
                          } else {
                            throw 'Could not launch $url';
                          }
                        }
                      }
                    ),
                    isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,))) : Stack()
                  ],
                )
                : Image.asset('imagens/2.gif', width: 150, height: 150,)
              ),
            ),
          ),
          isLoading ? Container(color: Colors.white, child: Center(child: Image.asset('imagens/2.gif', width: 150, height: 150,)))
          : Stack()
        ],
      ),
      drawer: const Menu(),
      bottomNavigationBar: 
      ConvexAppBar(
        curveSize: 100,
        top: -15,
        style: TabStyle.fixedCircle,
        backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
        items: 
        [
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.home, color: Colors.white,), 
              onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Sivcor()));},
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_back_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goBack();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.apps, color: Colors.white,),
              onPressed: () => _scaffoldKey.currentState!.openDrawer(),
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
              onPressed: () 
              {
                _webViewController.goForward();
              },
            )
          ),
          TabItem(
            icon: 
            IconButton(
              icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
              onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
            )
          ),
        ],
      ) 
    ),
  );

  @override
  void dispose() {
    super.dispose();
  }
} 