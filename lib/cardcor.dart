// ignore_for_file: avoid_print, unnecessary_null_comparison, unused_local_variable

import 'dart:async';

import 'package:clipboard/clipboard.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'ferramentas/menu.dart';
import 'package:whatsapp_unilink/whatsapp_unilink.dart';
import 'package:url_launcher/url_launcher.dart';
import 'alerta.dart';
import 'api/apiaddcard.dart';

class CardCor extends StatefulWidget {
  const CardCor({Key? key, required this.title}) : super(key: key); 

  static Future<String?> get _cartao async {

  var prefs = await SharedPreferences.getInstance();
  String cartao = (prefs.getString("cartao") ?? "");

    await Future.delayed(const Duration(seconds: 1));
    print('Resposta: $cartao');
    return cartao;
  }  

  static Future<String?> get _cdCor async {

  var prefs = await SharedPreferences.getInstance();
  String? cartao = (prefs.getString("cartao"));

    await Future.delayed(const Duration(seconds: 1));
    if(cartao == null){
      return null;
    }
    else{
    return cartao;
    }
  }  

  final String title;

  @override
  _CardCorState createState() => _CardCorState();
}

class _CardCorState extends State<CardCor> {

  

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _checkBoxVal = false;

  uuuu()async{
    final uuu = await CardCor._cdCor;
    if (uuu == null) {
      alert5(context, 'Para usar o CardCor é necessário ativar!');
    }
  }

  @override
  initState() {
    super.initState();
    setState(() {
      va = false;
    });
    uuuu();
  }

  final RoundedLoadingButtonController _btnController = RoundedLoadingButtonController();

  void _doSomething() async {
    Timer(const Duration(seconds: 2), () {
      _btnController.reset();
    });
  }

  clearUsr() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SIMP Simuladores Online',
      theme: ThemeData.light(),
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        body: 
        Center(
          child: SizedBox(
            width: 500,
            child: Padding(
              padding: const EdgeInsets.only(right: 15, left: 15),
              child: _body(context),
            ),
          ),
        ),
        drawer: const Menu(),
        bottomNavigationBar:
        ConvexAppBar(
          curveSize: 100,
          top: -15,
          style: TabStyle.fixedCircle,
          backgroundColor: const Color.fromRGBO(52, 58, 64, 1),
          items: 
          [
            TabItem(
              icon: 
              IconButton(
                icon: const Icon(Icons.home, color: Colors.white,), 
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const CardCor(title: '',)));},
              )
            ),
            TabItem(
              icon: 
              IconButton(
                icon: const Icon(Icons.apps, color: Colors.white,),
                onPressed: () => _scaffoldKey.currentState!.openDrawer(),
              )
            ),
            TabItem(
              icon: 
              IconButton(
                icon: const Icon(Icons.exit_to_app, color: Colors.white,), 
                onPressed: (){alert4(context, "Ao sair, não receberá mais notificações...");},
              )
            ),
          ],
        ) 
      ),
    );
  }



  _body(BuildContext context) {
  return Form(
    key: _formKey,
    child: Padding(
      padding: const EdgeInsets.only(left: 25, right: 25),
      child: 
          ListView(
            children: 
              [
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Image.asset('imagens/card.png', width: 55, height: 55, alignment: Alignment.center,),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 15, bottom: 10),
                  child: Text('Link do seu cartão de visita digital:', style: TextStyle(color: Color.fromRGBO(108, 48, 255, 1), fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                ),
                Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Container(
                    decoration: const BoxDecoration(color: Colors.white,),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child:
                      FutureBuilder(
                        future: CardCor._cartao,
                        builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                        ? Text(snapshot.data, style: TextStyle(color: Colors.grey[600]), textAlign: TextAlign.center,)
                        : const CircularProgressIndicator()
                      ),
                      /*Text('http://meucartao.in/v078l', style: TextStyle(color: Colors.grey), textAlign: TextAlign.center,),*/
                    ),
                  ),
                ),
                FutureBuilder(
                  future: CardCor._cartao,
                  builder: (BuildContext context, AsyncSnapshot snapshot) => snapshot.hasData
                  ? TextButton(onPressed: () {FlutterClipboard.copy(snapshot.data);}, 
                  child: 
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FaIcon(FontAwesomeIcons.copy, color: Colors.grey[600],),
                        Text('  Clique para Copiar', textAlign: TextAlign.center, style: TextStyle(color: Colors.grey[600]),),
                      ],
                    ),
                  )
                  : const CircularProgressIndicator()
                ),
                /*Text('Clique para Copiar', textAlign: TextAlign.center, style: TextStyle(color: Colors.grey[400]),),*/
                const Padding(
                  padding: EdgeInsets.only(right: 10, left: 10, bottom: 6),
                  child: Divider(),
                ),
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text('Preencha os dados para compartilhar:', style: TextStyle(color: Color.fromRGBO(108, 48, 255, 1), fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                ),
                textFormFieldLogin(),
                textFormFieldSenha(),
                //checkBut(),
                containerButton(context),
                containerButton2(context),
              ]
          )
    )
  );
}

  final controledetexto = TextEditingController();

  final controledesenha = TextEditingController();


textFormFieldLogin() {
  String valor ="";
  return Padding(
    padding: const EdgeInsets.only(top: 15, bottom: 20),
    child: TextFormField(
      scrollPadding: const EdgeInsets.all(0),
      focusNode: FocusNode(canRequestFocus: false, descendantsAreFocusable: false,),
      controller: controledetexto,
      onChanged: (text) {valor = text; print("O Texto: $text");},
      keyboardType: TextInputType.text,
      style: TextStyle(color: Colors.grey[600]),
      decoration: InputDecoration( 
        labelText: "Nome",
        labelStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
        hintText: "Nome do Cliente",
        contentPadding: const EdgeInsets.all(2),
        prefixIcon: const Icon(Icons.account_circle),
        enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
        focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      )
    ),
  );  
}

bool va = false;

textFormFieldSenha() {
  return TextField(
    controller: controledesenha, 
    onChanged: (_){
      setState(() {
        va = true;
      });
    },
    maxLength: 16,
    decoration: InputDecoration( 
      helperStyle: TextStyle(color: Colors.grey[50]),
      hintStyle: TextStyle(color: Colors.grey[600]),
      labelText: "WhatsApp",
      labelStyle: TextStyle(color: Colors.grey[600], fontSize: 14),
      hintText: "DDD + Número",
      contentPadding: const EdgeInsets.all(2),
      prefixIcon: const Icon(Icons.phone),
      enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
    )
  );
}

checkBut(){
  return Padding(
    padding: const EdgeInsets.only(top: 5),
    child: Row(
      children: [
        Checkbox(value: _checkBoxVal, onChanged: (bool? value) {setState(() => _checkBoxVal = value!);}, ),
        Text('Salvar no B2Cor', style: TextStyle(color: Colors.grey[600], fontSize: 12.0,),),
      ],
    ),
  );
}

containerButton(BuildContext context) {
  final wh = controledesenha.text;
  return TextButton(
    child: ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        color: Colors.green,
        width: 500,
        height: 50,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Padding(
                padding: EdgeInsets.only(left: 15),
                child: FaIcon(FontAwesomeIcons.whatsapp, color: Colors.white,),
              ),
              Padding(
                padding: EdgeInsets.only(right: 15),
                child: Text("  Compartilhar no WhatsApp", style: TextStyle(color: Colors.white, fontSize: 12.0)),
              ),          
            ],
          ),
        ),
      ),
    ),
    onPressed: () 
    {
      if(va == false){
        alert2(context, 'Preencha os Dados');
        setState(() {
          va = false;
        });
      }
      else {
        if(_checkBoxVal == true){
          _onClickLogin(context); 
          launchWhatsApp(); 
          _doSomething();
          setState(() {
            va = false;
          });
        } 
        else{
          launchWhatsApp(); 
          _doSomething();
          setState(() {
            va = false;
          });
        }
      }
      
    },
  );
}

containerButton2(BuildContext context) {
  return TextButton(
    child: ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        color: Colors.blue,
        width: 500,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: FaIcon(FontAwesomeIcons.share, color: Colors.white,),
            ),
            Padding(
              padding: EdgeInsets.only(right: 15),
              child: Text("  Compartilhar com Outro App", style: TextStyle(color: Colors.white, fontSize: 12.0)),
            ),          
          ],
        ),
      ),
    ),
    onPressed: () { _doSomething(); if(_checkBoxVal == true){_onClickLogin(context); share();} else{share();}},
  );
}


  Future<void> share() async {
    var nm = controledetexto.text;
    var prefs = await SharedPreferences.getInstance();
    String cartao = (prefs.getString("cartao") ?? "");
    await FlutterShare.share(
        title: 'Compartilhar Cartão',
        text: 'Olá $nm, veja meu cartão:',
        linkUrl: cartao,
        chooserTitle: ' ');
  }

  launchWhatsApp() async{
    var wts = controledesenha.text;
    var nm = controledetexto.text;
    var prefs = await SharedPreferences.getInstance();
    String cartao = (prefs.getString("cartao") ?? "");
    final link = WhatsAppUnilink(
      phoneNumber: '+55$wts',
      text: 'Olá $nm, veja meu cartão: $cartao',
    );
    await launch('$link');
  }



_onClickLogin(BuildContext context) async {
  var prefs = await SharedPreferences.getInstance();
  String id = (prefs.getString("id") ?? "");
    final nome = controledetexto.text;
    final fone = controledesenha.text;
    final responsavel = id;
    print("Nome: $nome , Celular: $fone , Responsavel: $id");      

    if( nome.isEmpty ){
      alert2(context, "Preencha os Dados");
     }
     else if( fone.isEmpty ){
      alert2(context, "Preencha os Dados");
     } 
     else {
       var usuarioCard = await ApiCard.login(nome,fone,responsavel);
       if( usuarioCard != null ) {
          alertAddLead(context, 'Indicação Adicionada');
        }
       else {
        alertNoAddLead(context, 'Indicação Não Adicionada');
       }    
     }
  }
}