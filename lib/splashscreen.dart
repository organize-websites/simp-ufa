
 // ignore_for_file: avoid_print
 
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'absoluta.dart';
import 'package:video_player/video_player.dart';

import 'homescreen.dart';

Future<void> main() async {
  var prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString('tokenjwt');
  print(token);
  runApp(MaterialApp(home: token == null ? const SplashPage() : const Abs()));
}

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  paraOutraTela() async{
    var _duration = const Duration(seconds: 1);
    return Timer(_duration, main);
  }
  late VideoPlayerController _controller;
  void navigationToNextPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) =>  const HomePage(title: '',)));
    /*Navigator.pushReplacementNamed(context, '/HomePage');*/
  }

  startSplashScreenTimer() async {
    var _duration = const Duration(seconds: 11);
    return Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    paraOutraTela();
    startSplashScreenTimer();
    _controller = VideoPlayerController.asset('imagens/s.mp4')..initialize().then((_) {_controller.play(); setState(() {});});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

    return Container(
      color: Colors.white,
      child: Center(
                child: _controller.value.isInitialized
                ? AspectRatio(aspectRatio: _controller.value.aspectRatio, child: VideoPlayer(_controller),)
                : Container()
                ),
    );
              
  }
}